rm -rf /usr/share/nginx/html/searchape-slave
yum remove httpd -y

alias ifconfig="/sbin/ifconfig"

wget http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
rpm -ihv epel-release-6-8.noarch.rpm
rm -rf epel-release-6-8.noarch.rpm

yum install epel-release -y
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
rpm -Uvh http://repo.mysql.com/mysql-community-release-el6-5.noarch.rpm

yum --enablerepo=remi-php72 install php -y
yum --enablerepo=remi-php72 install php-mysql php-xml php-soap php-xmlrpc php-mbstring php-json php-gd php-mcrypt -y

yum install htop -y

yum install nginx -y

mkdir -p /usr/share/nginx/html/searchape-slave
mv searchape-slave-master/* /usr/share/nginx/html/searchape-slave
rm -rf searchape-slave-master

# Prove that, doesnt forgive
yum install cronie -y
echo '*/1 * * * * wget -O /dev/null http://localhost/work
*/1 * * * * sleep 10; wget -O /dev/null http://localhost/work
*/1 * * * * sleep 20; wget -O /dev/null http://localhost/work
*/1 * * * * sleep 30; wget -O /dev/null http://localhost/work
*/1 * * * * sleep 40; wget -O /dev/null http://localhost/work
*/1 * * * * sleep 50; wget -O /dev/null http://localhost/work
*/5 * * * * wget -O /dev/null http://localhost/handy_hook' | crontab
# usar un cron para que se auto actualice en la version de github

# Prove that, doesnt forgive


service nginx start
chkconfig nginx on

yum --enablerepo=remi-php72 install php-fpm -y

rm -rf /etc/nginx/conf.d/default.conf
touch /etc/nginx/conf.d/default.conf

echo 'server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  _;
    root         /usr/share/nginx/html/searchape-slave;

    #include /etc/nginx/default.d/*.conf;

    index index.php;
    location / {
        try_files $uri $uri/ /index.php;
    }

    error_page 404 /404.html;
        location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }

    location ~ .php$ {
        try_files $uri =404;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
     }
}' >> /etc/nginx/conf.d/default.conf

iptables -F ##borra toda las reglas de iptable
#iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT



#iptables -
#iptables --line -vnL
#iptables -I INPUT 5 -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
#iptables --line -vnL
#service iptables save
#service iptables restart

service nginx restart
service php-fpm restart

#iptables -I INPUT 5 -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
#service iptables save
#service iptables restart

setsebool httpd_can_network_connect 1

php -v
nginx -v
