<?php 
require_once "src/slave.php";
class SCAPI{
	public $myip;
	
	public function __construct(){
		$this->myip = $this->myLocalIP();

	}
	public function myLocalIP(){
		$config = shell_exec("/sbin/ifconfig");
		preg_match_all("/inet (.*?) /", $config, $matches);
		
		$ips = array();
		foreach ($matches[1] as $key => $value){
			$value = str_replace(array("addr:"), "", $value);
			if ($value != "127.0.0.1")
				$ips[] = $value;
		}
	
		if(count($ips) > 1){
			// [SEND LOG] there are a lot of ips
			return "0.0.0.0";
		} else {
			return $ips[0];
		}
	}
	function getFromProxy($url) {
	      $curl = curl_init();            
	      curl_setopt($curl, CURLOPT_URL, $url); 
	      curl_setopt($curl, CURLOPT_PROXY, 'http://zproxy.luminati.io:22225');
	      curl_setopt($curl, CURLOPT_PROXYUSERPWD, 'lum-customer-hl_ef988d0c-zone-localhost-country-us:at5o8ztkusru');    
	      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);     
	      curl_setopt($curl, CURLOPT_HEADER, 0); 
	      curl_setopt($curl, CURLOPT_TIMEOUT, 60);
	      curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");           
	      $source = curl_exec ($curl);
	      curl_close ($curl);
	      $html = preg_replace('/^\s+|\n|\r|\s+$/m', '', $source);
	      return $html;
	}
	function connectAPI($ip_uncle, $params){
		$endpoint = "http://".$ip_uncle."/api/v1.0.0/";
		$data = $this->CurlPost_refer($endpoint, "http://192.168.0.23/esclavo.php", $params);
		return $data;
	}
	function sc_multi_size($array){
	    $node_count = count($array);
	    $curl_arr = array();
	    $master = curl_multi_init();

	    foreach ($array as $key => $value) {
	    	$url = "http://".$_SERVER["HTTP_HOST"]."/work/".urlencode($value);
		    $curl_arr[$key] = curl_init($url);
		    curl_setopt($curl_arr[$key], CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($curl_arr[$key], CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36');
		    curl_setopt($curl_arr[$key], CURLOPT_URL, $url);
		    curl_setopt($curl_arr[$key], CURLOPT_ENCODING, 'gzip,deflate');
		    curl_setopt($curl_arr[$key], CURLOPT_AUTOREFERER, true);
		    curl_setopt($curl_arr[$key], CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($curl_arr[$key], CURLOPT_TIMEOUT, 200);
		    curl_setopt($curl_arr[$key], CURLOPT_SSL_VERIFYPEER, FALSE);     
		    curl_setopt($curl_arr[$key], CURLOPT_SSL_VERIFYHOST, 2);

	      	curl_multi_add_handle($master, $curl_arr[$key]);
	    }

	    /*for($i = 0; $i < $node_count; $i++){
	        
	    }*/
	    $active = null;
	    do {
	        $mrc = curl_multi_exec($master, $active);
	    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

	    while ($active && $mrc == CURLM_OK) {
	        if (curl_multi_select($master) == -1) {
	            usleep(1);
	        }

	        do {
	            $mrc = curl_multi_exec($master, $active);
	        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
	    }

	    
	    foreach ($array as $key => $value) {
	        $results = curl_multi_getcontent($curl_arr[$key]);
	        $size[$key] = array($results);
	    }
	    return $size;
	    flush();
	}
	function getData($url,$agent = false){
        $curl = curl_init();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header["REMOTE_ADDR"] = "179.6.219.".rand(0,99);
        $header["HTTP_X_FORWARDED_FOR"] = "179.6.219.".rand(0,99);
        $header[] = "Pragma: "; // navegadores mantener este espacio en blanco.
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);     
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

		if($agent){
			curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
		}else{
			curl_setopt($curl, CURLOPT_USERAGENT, random_uagent());
		}	
		
		//curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
		//Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36

        $html = curl_exec($curl); 
        curl_close($curl);
        $html = preg_replace('/^\s+|\n|\r|\s+$/m', '', $html);
        return $html;
	}
	public function CurlPost_refer($url, $rf, $form) {
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_POST, true);  // Tell cURL you want to post something
	    curl_setopt($curl, CURLOPT_POSTFIELDS, $form);
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    curl_setopt($curl, CURLOPT_REFERER, $rf);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)");
	    curl_setopt($curl, CURLOPT_URL, $url);
	    $source = curl_exec ($curl);
	    curl_close ($curl);
	    return $source;
	}
}
