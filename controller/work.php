<?php
require_once "src/scapi.php";
// refrescar cada 2 minuto
//require_once "slave.php";
class Work extends SCAPI{
	private $widget;
	private $query;

	private $pretty;
	private $ip_uncle;
	
	private $get;

	public function __construct(){
		parent::__construct();
		$r = explode("/", $_SERVER["REQUEST_URI"]);
		$r = array_filter($r);
		$array = array();
		foreach ($r as $key => $value) {
			$array[] = $value;
		}
		$r = $array;
		if (count($r) == 1) { // get works
			$this->get = false;
		} else if (count($r) == 2) { //work query
			$this->get = true;
			$this->query = urldecode($r[1]);
		} else {
			throw new Exception("Error: Ruta para 'work' esta muy mal");	
		}	
	}
	public function checkWork(){
		$to = "http://".$this->ip_uncle."/api/v1.0.0/";
		$referer = "http://".$this->myip;
		$data = $this->CurlPost_refer($to, $referer, "ip=".$this->myip."&action=gettask");
		echo $data;
		return json_decode($data);
	}
	public function finishProcessQuery(){
		$to = "http://".$this->ip_uncle."/api/v1.0.0/";
		$referer = "http://".$this->myip;
		$data = $this->CurlPost_refer($to, $referer, "query=".urlencode($this->query)."&action=finish_query");
		echo $data;
	}
	private function prepareCheckJson($checkJson){
		$cj = json_decode($checkJson);
		$cj = $cj->main_page->web;
		foreach ($cj as $key => $value) {
			$arr[] = mytrim($value->u);
		}
		return $arr;
	}
	private function yahooScrapper($page="", $checkJson=""){
		global $page;
		$api = "https://search.yahoo.com/search?p=".urlencode($this->query)."&b=".decode_pageYahoo("8");
		$data = $this->getData($api);
		preg_match_all('#<h3 class="title"><a class=" ac-algo fz-l ac-21th lh-24"(.*?)"(.*?)>(.*?)</a></h3>#',$data,$t);
		preg_match_all('#<p class="lh-16">(.*?)</p>#',$data,$d);
		preg_match_all('#<a class=" ac-algo fz-l ac-21th lh-24" href="(.*?)"#',$data,$u);
		$total = count($t[1]);
		$arr = $this->prepareCheckJson($checkJson);
		for ($i=0; $i < $total; $i++) {
			$array[$i]["u"] = clean_urls($u[1][$i],"yahoo");		
			$array[$i]["t"] = html_human(strip_tags($t[3][$i]));
			$array[$i]["c"] = html_human(strip_tags($d[1][$i]));		
			$array[$i]["s"] = "yahoo";
			if(strpos($array[$i]["u"],"cbclk") | strlen($array[$i]["c"]) < 30 | in_array(mytrim($array[$i]["u"]), $arr) ) {
				unset($array[$i]);
			}
		}
		return array_values($array);
	} 
	private function bingScrapper($page="", $checkJson = ""){
		$api  = "https://www.bing.com/search?q=".urlencode($this->query)."&first=61";
		$data = $this->getData($api);
		$data = str_replace(array('<li class="b_algo"><h2>'),array("<start>"),$data);
		$data = preg_replace("#<cite>(.*?)</cite>#","",$data);
		$data = preg_replace("#<span class=\"vthv b_foregroundText\"><span>(.*?)</span></span>#","",$data);
		$data = preg_replace("#<span class=\"b_lLeft\">(.*?)</span>#", "", $data); //remueve la duracion de videos de yt
		preg_match_all('#<start><a href="(.*?)" h="(.*?)">(.*?)</a></h2>(.*?)</div></li>#s',$data,$out);

		$total = count($out[1]);
		$content = array();

		$arr = $this->prepareCheckJson($checkJson);

		for ($i=1; $i < $total; $i++) {
			$array[$i]["u"] = $out[1][$i];
			$array[$i]["t"] = html_human(strip_tags($out[3][$i]));
			$array[$i]["c"] = smart_web_content_result(html_human(strip_tags($out[4][$i])),"bing");
			$array[$i]["s"] = "bing";
			if(in_array(mytrim($array[$i]["u"]), $arr) ) {
				unset($array[$i]);
			}
		}
		return array_values($array);
	}
	private function duckScrapper($page="", $checkJson = ""){
		$api = "https://duckduckgo.com/html/?q=".$this->query;
		//$data = $this->getFromProxy($api);
		//$data = $this->getData($api);
		//if(empty($data)){
			$data = $this->getFromProxy($api);
		//}
		preg_match_all('#<div class="result results_links results_links_deep web-result "><div class="links_main links_deep result__body"> <!-- This is the visible part --><h2 class="result__title"><a rel="nofollow" class="result__a" href="(.*?)">(.*?)</a></h2><a class="result__snippet" href="(.*?)">(.*?)</a><div class="result__extras"><div class="result__extras__url"><span class="result__icon"><a rel="nofollow" href="(.*?)"><img class="result__icon__img" width="16" height="16" alt=""src="(.*?)" name="i15" /></a></span><a class="result__url" href="(.*?)">(.*?)</a>#',$data,$out);
		$total = count($out[1]);
		$arr = $this->prepareCheckJson($checkJson);
		for ($i=0; $i < $total; $i++) {
			$array[$i]["u"] = clean_urls($out[1][$i]);
			$array[$i]["t"] = html_human(strip_tags($out[2][$i]));
			$array[$i]["c"] = html_human(strip_tags($out[4][$i]));
			$array[$i]["s"] = "duck";
			if(in_array(mytrim($array[$i]["u"]), $arr) | strpos($array[$i]["u"],"y.js")) {
				unset($array[$i]);
			}
		}
		return array_values($array);
	}
	private function askScrapper($page="", $checkJson = ""){
		$api = "https://www.ask.com/web?q=".urlencode($this->query)."&page=5";
		$data = $this->getData($api);

		if(!strpos($data, "PartialSearchResults-item-url")){
			$api = "https://www.ask.com/web?q=".urlencode($this->query)."&page=3";
			$data = $this->getData($api);
		}

		if(!strpos($data, "PartialSearchResults-item-url")){
			$api = "https://www.ask.com/web?q=".urlencode($this->query)."&page=1";
			$data = $this->getData($api);
		}
		$data = preg_replace("#<p class=\"PartialSearchResults-item-url\">(.*?)</p>#","$1" , $data);
		$data = preg_replace('#<img (.*?)><div class="PartialSearchResults-youtube-play-btn">#','',$data);
		$data = preg_replace('#data-unified=\'(.*?)\'#','',$data);
		$data = preg_replace('#data-unified=\'(.*?)\'#','',$data);
		$data = str_replace('target="_blank" ','',$data);
		$data = strip_tags($data, '<p><div><a>');
		preg_match_all('#<div class="PartialSearchResults-item" data-zen="true"><div class="PartialSearchResults-item-title"><a class="PartialSearchResults-item-title-link result-link"href=\'(.*?)\'  rel=\'(.*?)\'>(.*?)</a>#',$data,$out); // 1 URL , 3 TITULO
		preg_match_all('#class="PartialSearchResults-item-abstract">(.*?)<#', $data, $desc);

		$total = count($out[1]);
		$arr = $this->prepareCheckJson($checkJson);

		for ($i=0; $i < $total; $i++) {
			$array[$i]["u"] = clean_urls($out[1][$i],"ask");
			$array[$i]["t"] = html_human(strip_tags($out[3][$i]));
			$array[$i]["c"] = smart_web_content_result(strip_tags($desc[1][$i]),"ask");
			$array[$i]["s"] = "ask";
			if(in_array(mytrim($array[$i]["u"]), $arr) ) {
				unset($array[$i]);
			}
		}
		return array_values($array);
	}
	private function aolScrapper($page="", $checkJson = ""){
		$api = "https://search.aol.com/search;?b=01&q=".urlencode($this->query);
		$data = $this->getFromProxy($api);
		preg_match_all('#lh-24" href="(.*?)"(.*?)>(.*?)</a></h3> <div><span class=" fz-ms fw-m fc-12th wr-bw lh-17">(.*?)</span></div></div><div class="compText aAbs" ><p class="lh-16">(.*?)</p>#si',$data,$out);
		$arr = prepare_check_json($checkJson);
		foreach ($out[1] as $key => $value) {
			$array[$key]["u"] = clean_urls($out[1][$key],"aol");
			$array[$key]["t"] = strip_tags($out[3][$key]);
			$array[$key]["c"] = smart_web_content_result(html_human(strip_tags($out[5][$key])));
			$array[$key]["s"] = "aol";
			if(in_array(mytrim($array[$key]["u"]), $arr) ) {
				unset($array[$key]);
			}
		}
		$array[($key+1)] = get_widgets($data);
		return array_values($array);
	}
	public function getWebResult($server, $page = "", $checkJson = ""){
		switch ($server) {
			case 'yahoo': 	$data = $this->yahooScrapper($page, $checkJson); 	break;
			case 'bing': 	$data = $this->bingScrapper($page, $checkJson); 	break;
			case 'duck':	$data = $this->duckScrapper($page, $checkJson);		break;
			case 'ask':		$data = $this->askScrapper($page, $checkJson);		break;
			case 'aol':		$data = $this->aolScrapper($page, $checkJson);		break;
			default: 		throw new Exception("Server is not allowed"); 		break;
		}
		return $data;
	}

	public function getWeb($servers){
		$checkjson = $this->connectAPI($this->ip_uncle, "action=search&q=".$this->query);
		$s = explode(",", $servers);
		$page = 0;
		foreach ($s as $server) {
			$array[$server] = $this->getWebResult($server, $page, $checkjson);
		}

		$result = array();
		foreach ($array as $value) {
			if(empty($value)) continue;
	    	$result = array_merge($result, $value);
		}
		
		$this->widget = end($array["aol"]);
		$temp = array_unique(array_column($result, 'u'));
		$array = array_intersect_key($result, $temp);
		$narray = array_chunk($array, 10);
		//paginacion con ahorro de caracteres
		/*foreach ($narray as $yx => $value) {	
			foreach ($value as $key => $row) {
				
				$ar[$yx]["u"][$key] = $row["url"];			
				$ar[$yx]["t"][$key] = $row["title"];
				$ar[$yx]["c"][$key] = $row["content"];
				$ar[$yx]["s"][$key] = $row["source"];
				if(empty($ar[$yx]["u"][$key])) unset($ar);
			}
		}*/
		return $narray;
	}	
	public function getImages(){
		$api = "https://images.search.yahoo.com/search/images;_ylt=?p=".urlencode($this->query);
		$data = $this->getData($api);
		preg_match_all('#<li class="ld " data=\'(.*?)\'  data-bns#', $data, $out);
		foreach ($out[1] as $key => $value) {
			$data = json_decode($value);
			$content["t"][$key] = html_human($data->alt);
			$content["f"][$key] = $data->iurl;
			$content["u"][$key] = $data->rurl;
			$content["w"][$key] = $data->w;
			$content["h"][$key] = $data->h;
			$content["s"][$key] = $data->s;
			$content["i"][$key] = clean_urls($data->ith,"get_images");
			$content["tw"][$key]    = $data->tw;
			$content["th"][$key]    = $data->th;
		}
		return $content;
	}
	private function getKey(){
		$r = rand(0,30);
	    switch ($r) {
	      case '0': $key = "AIzaSyB8gQZeqcEvuHIQF6zesXziFAESVCx_U2g"; break;
	      case '1': $key = "AIzaSyCmnn_zGQBFF5LEcmjX14tAEgV5phmhlKU"; break;
	      case '2': $key = "AIzaSyDdEx51Pvl4_qrky6vNx1L1hV-SmA6kQsQ"; break;
	      case '3': $key = "AIzaSyBwGVq5f4wGmy9jMqjZT68I_C0TefuVGo0"; break;
	      case '4': $key = "AIzaSyC17taD6R3G7Yj5sp2KsEn5HfYFd83PSNs"; break;
	      case '5': $key = "AIzaSyCiRJV43ySSLBh5-AVvU0HcKYNPXoXardU"; break;
	      case '6': $key = "AIzaSyC5MU8ewjmDMhXhlEVZg0EBrJxXuvOJ6pU"; break;
	      case '7': $key = "AIzaSyAitjQBTabsH_Lc4zJj9TWps9_Qnw57CUk"; break;
	      case '8': $key = "AIzaSyAmMxQvVpCExCIvcdxlx0bbfBmcU8BmmGU"; break;
	      case '9': $key = "AIzaSyA6fFsrNXAhs3DycB7VtX8UqVf8qBHKGNc"; break;
	      case '10': $key = "AIzaSyA_-vflOEWTJpI4GgwVBnUUpTzwG9mTInE"; break;
	      case '11': $key = "AIzaSyDimB_XN0ohYIacxt2PuiksCsa0dkPl9SE"; break;
	      case '12': $key = "AIzaSyBVkCcAbTWf5bU5w-dsiuxymVu76F7YpTM"; break;
	      case '13': $key = "AIzaSyA6yvVkxT1kb6j70J6pLO4_gsdzBl_VenI"; break;
	      case '14': $key = "AIzaSyAa-QSo6t4VQo1diI7_Udqoa_iUE0OqoHE"; break;
	      case '15': $key = "AIzaSyDW38Vh9hOPseYG2WACbH1HAtZgIQgs-14"; break;
	      case '16': $key = "AIzaSyB94OCb0UUT7zPbpF_gAeP-scn_bSM7ctA"; break;
	      case '17': $key = "AIzaSyCazfn5ls-yahBEOqpXNb3BLqFZBkLyXNg"; break;
	      case '18': $key = "AIzaSyCtervpGO-WVR_6018klsvMBci4XgJO2r4"; break;   
	      case '19': $key = "AIzaSyD_XjO3Pr0Rwbks5KO0OTduBdUMgYs5hkY"; break;
	      case '20': $key = "AIzaSyBIzF_1H3-lmYfXLUrI-KbnmeQQdAQSEJo"; break;
	      case '21': $key = "AIzaSyD_iwMfa7v2dpQZEPVLq5-t2TVabBSKCZ0"; break; 
	      case '22': $key = "AIzaSyBq3CA-ZtrkaKz-GbY3ejh6Bl1fdx4rWxU"; break;  
	      case '23': $key = "AIzaSyB96HcwLWGypWeTKqprP0KwKoYxjFSYpuU"; break;  
	      case '24': $key = "AIzaSyBlHJXMm-IEZDlqWvl4O6mwGrV_XYoyOrY"; break;
	      case '25': $key = "AIzaSyBLvYf3NlalIql8Eb7-P4NaRr1i6j4uUjU"; break;
	      case '26': $key = "AIzaSyBXPV-VB0s2ajWCl2Yr9dvpOFsRGq8hh3k"; break;
	      case '27': $key = "AIzaSyBmpkUrZx5Vo07e5CeGS-S8EKcUHJzwqWk"; break; 
	      case '28': $key = "AIzaSyDCH5Gxxg3A58maZnqpibiDgOe3uYpEEwE"; break; 
	      case '29': $key = "AIzaSyDmPoZ8x9HgJM-Lexn4VghJVCe8ans86t8"; break;
	      case '30': $key = "AIzaSyA5wZ5lM8OnjuVp8HsjvM0trjEVkehbR6E"; break;
	    }
  		return $key;
	}
	private function videosInfo($videoIDs, $getDescription = false){
        $return = array();
        $key = get_key();

        if (is_array($videoIDs) && count($videoIDs) > 0) {
            // Prepare request params for getting information of video
            $params = array(
                'id' => implode(',', $videoIDs),
                'key' => $key,
                'part' => 'snippet,contentDetails,statistics'
            );
            // Pack request params
            $request = http_build_query($params);
            // Send HTTP request
            if (is_string($remote = http("https://www.googleapis.com/youtube/v3/videos?{$request}")) &&
                is_array($items = object2Array(json_decode($remote))) &&
                isset($items['items']) && count($items['items']) > 0
            ) {
                foreach ($items['items'] as $key => $videoInfo) { 	
					$content["t"][$key] = $videoInfo["snippet"]["title"];
					$content["c"][$key] = $videoInfo["snippet"]["channelTitle"];
					$content["i"][$key] = $videoInfo["id"];
					$content["d"][$key] = parseDuration($videoInfo["contentDetails"]["duration"]);
					$content["h"][$key] = $videoInfo["statistics"]["viewCount"];
                }

            }
        }

        return $content;
	}
	public function getVideo(){
		$key = $this->getKey();
		$api = "https://www.googleapis.com/youtube/v3/search?key=".$key."&part=id&order=relevance&maxResults=30&q=".urlencode($this->query)."&type=video";
		$data = json_decode($this->getData($api));
		foreach ($data->items as $item) {
			$id = $item->id->videoId;
			$videoIDs[] = $id;
		}
		return $this->videosInfo($videoIDs,$this->query);
	}
	public function getWidgets($servers){

	}
	public function constructByType($type, $servers){
		if(!isset($this->query)) 	throw new Exception("Error: Missed query.");
		if(!isset($servers)) throw new Exception("Error: Missed servers.");
		
		$data = array();
		switch ($type) {
			case 'web': 	$data = $this->getWeb($servers); break;
			case 'image': 	$data = $this->getImages(); break;
			case 'video': 	$data = $this->getVideo(); break;
			//case 'widget': 	$data = $this->getWidgets(""); break;
			/*case 'csetoken':
				$u = urldecode($_POST["url"]);
				$u = explode("&sort=&googlehost",$u);
				$url = preg_replace("#q=(.*?)&#","q=\"+q+\"&",$u[0]);
				$params = "action=csetoken&url=".urlencode($url);
				$d = connect_api($params);
				print_r("response: ".$d);
				break;*/
			default: throw new Exception("Error: This type '$type' is not allowed");break;
		}

		return $data;
	}
	private function sendWeb($data){
		$w = $this->widget;
		if(!empty($w["wiki"]["i"]["t"])){
			$wj = json_encode($w,JSON_PARTIAL_OUTPUT_ON_ERROR);
			$params = "action=ucolumn&q=".urlencode($this->query)."&column=widgets&data=".urlencode($wj);
			$d = $this->connectAPI($this->ip_uncle, $params);
			echo "widget: $wj | ".$d."<hr>";
		}
		echo "<hr>";
		foreach ($data as $key => $value) {
			$json = json_encode($value,JSON_PARTIAL_OUTPUT_ON_ERROR);
			$params = "action=ucolumn&q=".urlencode($this->query)."&column=page_".($key+1)."&data=".urlencode($json);
			$d = $this->connectAPI($this->ip_uncle, $params);	
			echo "page $key: $json | ".$d."<hr>";
		}
	}
	private function sendImage($data){
		$json = json_encode($data,JSON_PARTIAL_OUTPUT_ON_ERROR);
		if ($json != "null") {
			$params = "action=ucolumn&q=".urlencode($this->query)."&column=images&data=".urlencode($json);
			$d = $this->connectAPI($this->ip_uncle, $params);
			echo "image: $json | ".$d."<hr>";
		}
	}
	private function sendVideo($data){
		$json = json_encode($data,JSON_PARTIAL_OUTPUT_ON_ERROR);
		if ($json != "null") {
			$params = "action=ucolumn&q=".urlencode($this->query)."&column=video&data=".urlencode($json);
			$d = $this->connectAPI($this->ip_uncle, $params);
			echo "video: $json | ".$d."<hr>";
		}
	}
	private function sendWidget($data){
		$json = json_encode($data,JSON_PARTIAL_OUTPUT_ON_ERROR);
		if(!empty($array["widget"])){
			$params = "action=ucolumn&q=".urlencode($q)."&column=widgets&data=".urlencode($json);
			createFile($q,$type,$params);
			$d = $this->connectAPI($params);
			echo $d;
		}
	}
	public function sendResult($type, $data){
		switch($type){
			case "web":    $this->sendWeb($data);    break;
			case "image":  $this->sendImage($data);  break;
			case "video":  $this->sendVideo($data);  break;
			case "widget": $this->sendWidget($data); break;
		}
	}
	public function constructResult($types, $servers){
		$types = explode(",", $types);
		foreach ($types as $type) {
			$data = $this->constructByType($type, $servers);
			$array[$type] = $data;
			$this->sendResult($type, $data);
		}   
		$data = ($this->pretty) ? json_encode($array,JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES) : $data = json_encode($array, JSON_PARTIAL_OUTPUT_ON_ERROR | JSON_UNESCAPED_SLASHES );
		return $data;
	}
	public function run($config){ // problemas al insertar
		$this->pretty = $config->pretty;
		$this->ip_uncle = $config->ip_uncle;
		if(!$this->get) {
			$data = $this->checkWork();
			if (count($data) == 0 ){
				echo "No hay nada por hacer x]";
				//require_once "handy_hook.php";
				// [SEND LOG] Send a message that I do nothing to do and update my resources
				//handy_hook($cpu, $ram, $state, $rank);
			} else {
				$this->sc_multi_size($data);
			}
		} else {
			//foreach ($data as $key => $value) {
				//$this->query = $value;
			    //$result = $this->constructResult("web", "aol");
				$result = $this->constructResult("image,web,video", "yahoo,duck,aol,ask,bing");
				//print_r($result);die;
			    //eliminar query de processing_querys y a el slave count_processing_querys bajarle 1
				$this->finishProcessQuery();
			//}
		} 
	}
}


// 1 - 3 problemas 3 puntos menos
// 201810457
// 201810288
// 201810666
// 201810190