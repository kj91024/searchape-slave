<?php 
require_once "src/scapi.php";
//require_once "slave.php";
class HandyHook extends SCAPI{
	private $config;
	function __construct(){
		parent::__construct();
	}
	function MemoryUsage(){
		$exec_free = explode("\n", trim(shell_exec('free')));
		$get_mem = preg_split("/[\s]+/", $exec_free[1]);
		$mem = round(($get_mem[2]+$get_mem[4])/$get_mem[1]*100, 0)+2;
	    return $mem;
	}
	function CPUUsage(){
		$exec_loads = sys_getloadavg();
		$exec_cores = trim(shell_exec("grep -P '^processor' /proc/cpuinfo|wc -l"));
		$cpu = round($exec_loads[1]/($exec_cores + 1)*100, 0);
	    return $cpu;
	}
	function SlaveInfo(){
		$to = "http://".$this->config->ip_uncle."/api/v1.0.0/";
		$referer = "http://".$this->myip;
		$data = CurlPost_refer($to, $referer, "ip=".$this->myip."&action=infoslave");
		$data = json_decode($data);
		return $data;
	}
	function Hook($rank, $state = -2){
		$cpu   = $this->CPUUsage();
		$ram   = $this->MemoryUsage();
		
		$to = "http://".$this->config->ip_uncle."/api/v1.0.0/";
		$referer = "http://".$this->myip;
		$param = "ip=".$this->myip."&cpu=$cpu&ram=$ram&rank=$rank&action=iuslaves";
		if ($state != -2) {
			$param .= "&state=$state";
		}

		$data = CurlPost_refer($to, $referer, $param);
		return $data;
	}
	function run($config){
		$this->config = $config;
		$info = $this->SlaveInfo();

		//verifico el estado en el que estoy y lo uso
		//$state = (count($info) == 0)? -1 : $info->state;
		// -1 significa esta esperando por queries para procesar.
		$data = (count($info) == 0) ? $this->Hook($this->config->rank, -1) : $this->Hook($this->config->rank);
		if ($data == "0") {
			// [SEND LOG] send an error log from the slave, because it happens that the API does not update the slave for any reason
		} else {
			echo "All is [OK]";
		}
	}
}

/* Auto actualizar los bots con esta url
9101141 es el ID del Projecto que esta debajo del titulo
https://gitlab.com/api/v4/projects/9101141/repository/commits?ref_name=master&since=2018-10-29T19:54:43.000Z
*/

// refrescar cada 5 minutos

