<?php
//Source
require_once "src/slave.php";
//require_once "src/scapi.php";
//require_once "src/sscrapper.php";

// Controller
require_once "controller/handyhook.php";
require_once "controller/work.php";
/*require_once "controller/upgrade.php";
require_once "controller/monitor.php";*/

try {
	$slave = new Slave();

	$slave->route("work", "Work"); 				// Trabajo
	$slave->route("handy_hook", "HandyHook"); 	// Gancho practico
	//$slave->route("upgrade", new Upgrade); 			// Actualizacion de sistema automaticamente
	//$slave->route("monitor", new Monitor); 		// Show all about Machine
	
	$slave->config_file("/usr/share/nginx/html/searchape-slave/config.json");

	$slave->run();
} catch(Exception $e) {
	print_r($e);
  	//echo 'Message: ' .$e->getMessage();
}